var data = {
  labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  series: [
    [1, 3, 7, 12, 16, 18, 28, 32, 46, 56]
  ]
};

var options = {
  low: 0,
  width: '500px',
  axisY: {

  },
  axisX: {

  },
};

document.addEventListener('DOMContentLoaded', function() {
  new Chartist.Line('#viewsGraph', data, options);
});
