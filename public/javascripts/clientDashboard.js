var data = {
  labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  series: [
    [1, 3, 7, 12, 16, 18, 28, 32, 46, 56]
  ]
};

var options = {
  low: 0,
  width: '155px',
  showArea: true,
  showPoint: false,
  axisY: {
    offset: 1,
    showLabel: false,
    showGrid: false
  },
  axisX: {
    offset: 1,
    showLabel: false,
    showGrid: false
  },
  chartPadding: {
    top: 5,
    right: 10,
    bottom: 0,
    left: -5
  },
};


var graphs = ['#allTimeGraph', '#monthlyGraph', '#dailyGraph', '#hourlyGraph'];

document.addEventListener('DOMContentLoaded', function() {
  for(i in graphs) {
    new Chartist.Line(graphs[i], data, options);
  }
});
