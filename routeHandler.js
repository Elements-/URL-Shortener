var express = require('express');
var router = express.Router();

var api = require('./routes/api');
var redirect = require('./routes/redirect');
var statistics = require('./routes/statistics');

// Homepage
router.get('/', function(req, res, next) {
  res.render('index');
});

// API for new URLs
router.get('/api/:url', function(req, res) {
  api.newUrl(req, res);
});

// Stats Panels
router.get('/stats/:id/:key/:page', function(req, res) {
  statistics.handle(req, res);
});

router.get('/stats/:id/:key', function(req, res) {
  statistics.handle(req, res);
});

// Serve redirect
router.get('/:id', function(req, res) {
  redirect.handle(req, res);
});

module.exports = router;
