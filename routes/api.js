var express = require('express');
var validator = require('validator');
var mongo = require('../mongoHandler');
var moment = require('moment');

exports.newUrl = function(req, res) {
  // validate base64
  if(!validator.isBase64(req.params.url)) {
    res.send('URL needs to be base64');
    return;
  }

  // validate is url
  var url = new Buffer(req.params.url, 'base64').toString('ascii');
  if(!validator.isURL(url)) {
    res.send('Invalid URL');
    return;
  }

  // check if the url already exists
  mongo.findDocuments('Links', { "url" : url }, function(docs) {
    if(docs.length == 0) {
      var id = randString();
    } else {
      res.send(address + docs[0].id);
      return;
    }
  });

  // generate url & add to db
  var data = {
    "id" : id,
    "time" : moment().unix(),
    "creatorIP" : req.connection.remoteAddress,
    "url" : url,
    "stats" : {
      "views" : {}
    }
  };
  mongo.insertDocument('Links', data, function(doc) {
    res.send(address + id);
  });
}


function randString(length) {
  return Math.floor(Math.random()*Math.pow(10,10)).toString(36).substring(0, length);
}
