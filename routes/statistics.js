var express = require('express');
var validator = require('validator');
var mongo = require('../mongoHandler');
var moment = require('moment');

var options = {
  "navElements" : ['Home', 'Statistics'],
  "pillElements" : [
    {
      "name" : "Dashboard",
      "icon" : "align-justify",
      "url" : "dashboard"
    },
    {
      "name" : "Views",
      "icon" : "bar-chart",
      "url" : "views"
    },
    {
      "name" : "Location Data",
      "icon" : "globe",
      "url" : "location"
    },
    {
      "name" : "Options",
      "icon" : "pencil",
      "url" : "options"
    }
  ]
};

exports.handle = function(req, res) {
  // check the id and key

  // TODO: change the req.parms to vars from above
  var result = options;
  result["url"] = address + 'stats/' + req.params.id + '/' + req.params.key;
  console.log(result["url"]);

  // dashboard
  if((!req.params.page) || req.params.page == 'dashboard' || req.params.page == 'index') {
    res.render('statsDashboard', result);
  } else if(req.params.page == 'views') {
    res.render('statsViews', result);
  }

  // get data / build page ...

  // render the template
}
