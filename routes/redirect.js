var express = require('express');
var validator = require('validator');
var mongo = require('../mongoHandler');
var moment = require('moment');

exports.handle = function(req, res) {
  var id = req.params.id;
  mongo.findDocuments('Links', { "id" : id }, function(docs) {
    // check if it exists
    if(docs.length == 0) {
      res.send('404, could not find that redirect.');
      return;
    }
    // stats
    var updatedDoc = addView(docs[0]);
    mongo.updateDocument('Links', { "id" : id }, updatedDoc);

    // redirect
    res.redirect(docs[0].url);
  });
}

function addView(doc) {
  var now = Math.floor(moment().unix() / 60);

  // increment values / create time field
  if(!doc["stats"]["views"][now]) {
    doc["stats"]["views"][now] = {
      "visits" : 1
    };
  } else {
    doc["stats"]["views"][now]["visits"] += 1;
  }

  return doc;
}
